# Jenkins API

### Assumptions
+ A single script (file) is required;
+ PHP version is 5.5 or above;
+ This script is to run on a command line and not on the browser, and will print a JSON containing the jobs list found (apart from saving it in the DB) to the console.


### Running
Modify the ```$settings``` variable to your likings (server URL and SQLite DB filename) and run the following:

```$ php jenkins-api.php```



Enjoy!
