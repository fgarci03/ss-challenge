<?php

$settings = [
    'jenkins_server' => 'http://filipegarcia.no-ip.org:8080',
    'db_name' => 'jenkins-ci.sqlite'
];


/**
 * JenkinsJobsList Class
 *
 * Get a list of a remote Jenkins server's jobs.
 * An SQLite DB is used to store each job, status, and lookup datetime.
 *
 * @package    JenkinsAPI
 * @author     Filipe Garcia <garcia.filipenadre@gmail.com>
 * @copyright GNU General Public License
 */
class JenkinsJobsList
{
    /**
     * The Jenkins' API URL.
     *
     * @var string
     */
    protected $api_url;

    /**
     * The Database object.
     *
     * @var PDO
     */
    protected $db;


    /**
     * Constructor.
     *
     * @param   array $config - Initialization settings
     */
    public function __construct($config)
    {
        $api_request = '/api/json?tree=jobs[name,lastBuild[result]]';
        $this->api_url = preg_replace('{/$}', '', $config['jenkins_server']) . $api_request; // remove the forward slash from the end of the url if it exists
        $this->db = $this->init_sqlite($config['db_name']);
    }

    /**
     * Gets a list of Jenkins jobs and saves them to the DB.
     *
     * @throws PDOException     If there is a problem inserting to the DB
     */
    public function save_jobs_to_db()
    {
        $jobs_list = $this->get_jobs_list();

        try {
            $query = $this->db->prepare('INSERT INTO jobs (job, status, datetime) VALUES (?, ?, ?);');

            foreach ($jobs_list['jobs'] as $job) {
                $query->execute(array($job['name'], $job['lastBuild']['result'], $jobs_list['datetime']));
            }

            echo sizeof($jobs_list['jobs']) . " jobs added to the DB!\n";
        } catch (PDOException $e) {
            die('Error inserting jobs to the DB: ' . $e->getMessage());
        }
    }

    /**
     * Gets a list of Jenkins jobs from the DB and outputs them in JSON.
     *
     * @throws PDOException     If there is a problem querying the DB
     */
    public function get_saved_jobs()
    {
        try {
            $query = $this->db->prepare('SELECT * FROM jobs;');
            $query->execute();

            $jobs_list = json_encode($query->fetchAll(PDO::FETCH_ASSOC), JSON_PRETTY_PRINT);
            echo "Jobs list in the DB:\n\n$jobs_list";
        } catch (PDOException $e) {
            die('Error querying data from the DB: ' . $e->getMessage());
        }
    }

    /**
     * Get the list of jobs and the lookup's datetime.
     * Try to use cURL, but fallback to get_file_contents if disabled or not installed.
     *
     * @return array
     */
    public function get_jobs_list()
    {
        if (function_exists('curl_version')) {
            $ch = curl_init($this->api_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $json_jobs_list = curl_exec($ch);
            curl_close($ch);
        } else {
            $json_jobs_list = file_get_contents($this->api_url);
        }

        $jobs_list = json_decode($json_jobs_list, true);
        $current_date = new DateTime('now', new DateTimeZone('UTC'));

        return [
            'jobs' => $jobs_list['jobs'],
            'datetime' => $current_date->format('U') * 1000 // get current date in UTC milliseconds
        ];
    }


    /**
     * Initialize the SQLite DB.
     *
     * @param   string $db_name
     * @return  PDO
     * @throws  PDOException     If there is a problem initializing the DB
     */
    private function init_sqlite($db_name)
    {
        try {
            $db = new PDO('sqlite:' . $db_name);
            $sql = '
              CREATE TABLE IF NOT EXISTS jobs (
                  id        INTEGER     PRIMARY KEY,
                  job       TEXT        NOT NULL,
                  status    TEXT        NOT NULL,
                  datetime  NUMERIC     NOT NULL
              );
            ';

            $db->exec($sql);
        } catch (PDOException $e) {
            die('Error initializing DB: ' . $e->getMessage());
        }

        return $db;
    }
}

$action = new JenkinsJobsList($settings);
$action->save_jobs_to_db();
