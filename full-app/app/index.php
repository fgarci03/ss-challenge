<?php

define('ROOT_DIR', realpath(dirname(__FILE__)) . '/');
define('APP_DIR', ROOT_DIR . 'application/');
$config = parse_ini_file(APP_DIR . 'data/config.ini');
define('BASE_URL', $config['base_url']);

require_once(APP_DIR . 'plugins/autoload.php');
require_once(ROOT_DIR . 'system/traits/loaders.php');
require_once(ROOT_DIR . 'system/traits/utils.php');
require_once(ROOT_DIR . 'system/extra/SecureSession.php');
require_once(ROOT_DIR . 'system/core/model.php');
require_once(ROOT_DIR . 'system/core/view.php');
require_once(ROOT_DIR . 'system/core/controller.php');
require_once(ROOT_DIR . 'system/core/helper.php');
require_once(ROOT_DIR . 'system/core/api.php');
require_once(ROOT_DIR . 'system/core/exceptions/api_exception.php');
require_once(ROOT_DIR . 'system/core/session.php');
require_once(ROOT_DIR . 'system/pip.php');

new Session();
pip();
