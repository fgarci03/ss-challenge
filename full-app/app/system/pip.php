<?php

function pip()
{
  global $config;

  // Set our defaults
  $controller = $config['default_controller'];
  $action = 'index';
  $url = '';

  // Get request url and script url
  $request_url = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
  $script_url = (isset($_SERVER['PHP_SELF'])) ? $_SERVER['PHP_SELF'] : '';

  // Get our url path and trim the leading and trailing slash
  if ($request_url != $script_url) $url = trim(preg_replace('/' . str_replace('/', '\/', str_replace('index.php', '', $script_url)) . '/', '', $request_url, 1), '/');
  $url = preg_replace('/\?.*/', '', $url);

  // Split the url into segments
  $segments = explode('/', $url);

  // Redirect to REST API or Controllers
  if (Utils::starts_with($url, 'api')) {
    try {
      if (isset($segments[1])) {
        $api_version = $segments[1];
        $path = APP_DIR . 'api/' . $api_version . '.php';
        if (file_exists($path)) {
          require_once($path);

          $endpoint = array_slice($segments, 2);
          $api = new $api_version($endpoint, $_SERVER['REMOTE_ADDR']);
          echo $api->processAPI();
        } else {
          throw new API_Exception('api version not implemented', 405);
        }
      } else {
        throw new API_Exception('no version specified', 405);
      }
    } catch (API_Exception $e) {
      header('HTTP/1.1 ' . $e->getCode());
      echo json_encode(Array('error' => $e->get_custom_message()));
    } catch (PDOException $e) {
      header('HTTP/1.1 500 Internal Server Error');
      echo json_encode(Array('error' => $e->getMessage()));
    } catch (Exception $e) {
      header('HTTP/1.1 ' . $e->getCode());
      echo json_encode(Array('error' => $e->getMessage()));
    }

  } else {
    // Do our default checks
    if (isset($segments[0]) && $segments[0] != '') $controller = $segments[0];
    if (isset($segments[1]) && $segments[1] != '') $action = $segments[1];

    $path = APP_DIR . 'controllers/' . $controller . '.php';
    if (file_exists($path)) {
      require_once($path);
    } else {
      $controller = $config['error_controller'];
      require_once(APP_DIR . 'controllers/' . $controller . '.php');
      $action = 'error404';
    }

    if (!method_exists($controller, $action) && !method_exists('_' . $controller, $action)) {
      $controller = $config['error_controller'];
      require_once(APP_DIR . 'controllers/' . $controller . '.php');
      $action = 'index';
    } else if (!method_exists($controller, $action) && method_exists('_' . $controller, $action)) {
      // some words are reserved as a class name. In those cases, append a '_' to the beginning of it's name and it
      // will be striped from the url
      $controller = '_' . $controller;
    }

    // Create object and call method
    $obj = new $controller;

    die(call_user_func_array(array($obj, $action), array_slice($segments, 2)));
  }
}
