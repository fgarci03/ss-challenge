<?php

/**
 * A generic utils trait.
 */
trait Utils
{
  /**
   * Escape string.
   *
   * @param string $string
   * @return string
   */
  public static function escape_string($string)
  {
    return htmlspecialchars($string);
  }

  /**
   * Recursively escape arrays.
   *
   * @param array $array
   * @return array
   */
  public static function escape_array(array $array)
  {
    array_walk_recursive($array, create_function('&$v', '$v = htmlspecialchars($v);'));
    return $array;
  }

  /**
   * Validate email addresses.
   *
   * @param string $email
   * @return boolean true if valid
   */
  public static function validate_email($email)
  {
    return !!filter_var($email, FILTER_VALIDATE_EMAIL);
  }

  /**
   * Validate regular expressions.
   *
   * @param string $string
   * @param string $regexp
   * @return boolean true if match
   */
  public static function validate_regexp($string, $regexp)
  {
    return !!preg_match($regexp, $string);
  }

  /**
   * Check if a string starts with another string.
   *
   * @param string $haystack
   * @param string $needle
   * @return boolean true if match
   */
  public static function starts_with($haystack, $needle)
  {
    return $needle === '' || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
  }

  /**
   * Check if a string ends with another string.
   *
   * @param string $haystack
   * @param string $needle
   * @return boolean true if match
   */
  public static function ends_with($haystack, $needle)
  {
    return $needle === '' || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
  }
}
