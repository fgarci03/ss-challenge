<?php

/**
 * Basic application loaders.
 */
trait Loaders
{
  /**
   * Load a helper.
   *
   * @param string $name
   * @return object the helper class
   */
  public static function load_helper($name)
  {
    require_once(APP_DIR . 'helpers/' . strtolower($name) . '.php');

    $hp = explode("/", $name);
    $helper_name = end($hp);
    $helper = new $helper_name;
    return $helper;
  }

  /**
   * Load a model.
   *
   * @param string $name
   * @return object the model class
   */
  public static function load_model($name)
  {
    require_once(APP_DIR . 'models/' . strtolower($name) . '.php');

    $mn = explode("/", $name);
    $model_name = end($mn);
    $model = new $model_name;
    return $model;
  }
}
