<?php

/**
 * Methods to be used in the views.
 */
class View
{
  /**
   * Controller variables to be used in the view.
   *
   * @var array $page_vars
   */
  private $page_vars = array();

  /**
   * The template to be used.
   *
   * @var string $template
   */
  private $template;


  /**
   * Constructor.
   *
   * @param string $template
   */
  public function __construct($template)
  {
    $this->template = APP_DIR . 'views/' . $template . '.php';
  }


  /**
   * Set variables in the view.
   *
   * @param string $var
   * @param mixed $val
   */
  public function set($var, $val)
  {
    $this->page_vars[$var] = $val;
  }

  /**
   * Render the view.
   */
  public function render()
  {
    extract($this->page_vars);

    ob_start();
    require($this->template);
    echo ob_get_clean();
  }
}
