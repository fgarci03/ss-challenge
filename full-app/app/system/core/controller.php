<?php

/**
 * Controller class.
 */
abstract class Controller
{
  use Loaders, Utils;

  /**
   * Load a view.
   *
   * @return View
   */
  public function load_view($name)
  {
    $view = new View($name);
    return $view;
  }

  /**
   * Redirect a page.
   */
  public function redirect($loc)
  {
    global $config;
    header('Location: ' . $config['base_url'] . $loc);
  }
}
