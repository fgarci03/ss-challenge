<?php

/**
 * Custom exception for the REST API.
 */
class API_Exception extends Exception
{
  /**
   * Custom error message.
   *
   * @var mixed $custom_message
   */
  private $custom_message;


  /**
   * Constructor.
   *
   * @param string $message
   * @param int $code
   * @param Exception $previous
   */
  public function __construct($message = 'Internal Server Error', $code = 500, Exception $previous = null)
  {
    parent::__construct(json_encode($message), $code, $previous);
    $this->custom_message = $message;
  }


  /**
   * Get a custom error message.
   *
   * @return mixed $custom_message
   */
  public function get_custom_message()
  {
    return $this->custom_message;
  }
}
