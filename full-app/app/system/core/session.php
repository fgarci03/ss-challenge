<?php

/**
 * Create a secure session.
 */
class Session
{
  /**
   * Constructor.
   */
  public function __construct()
  {
    session_start();

    if (!isset($_SESSION['sToken'])) {
      // Generate random session token if not already set
      $factory = new RandomLib\Factory;
      $generator = $factory->getLowStrengthGenerator();
      $_SESSION['sToken'] = $generator->generateString(32);
      $_SESSION['address'] = $_SERVER['REMOTE_ADDR'];
    }
  }
}
