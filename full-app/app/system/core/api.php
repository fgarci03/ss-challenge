<?php

/**
 * The REST API.
 */
abstract class API
{
  use Loaders, Utils;

  /**
   * Endpoint of the request.
   *
   * @var string $endpoint
   */
  private $endpoint;

  /**
   * The request object.
   *
   * @var array $request
   */
  protected $request;

  /**
   * The method of the request.
   *
   * @var string $method
   */
  protected $method;

  /**
   * The request URL.
   *
   * @var array $args
   */
  protected $args;

  /**
   * The request content.
   *
   * @var object $file
   */
  protected $file;

  /**
   * The available status codes.
   *
   * @var array $http_status
   */
  protected $http_status = [
    200 => 'OK',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    500 => 'Internal Server Error'
  ];

  /**
   * Build the API response.
   *
   * @param string $data
   * @param int $status
   * @return array
   */
  protected function build_response($data = '', $status = 200)
  {
    return [
      'data' => $data,
      'status' => $status
    ];
  }


  /**
   * Constructor.
   *
   * @param array $request_url
   * @throws API_Exception
   */
  function __construct(array $request_url)
  {
    // Enable CORS
    header('Access-Control-Allow-Orgin: *');
    header('Access-Control-Allow-Methods: *');
    header('Content-Type: application/json');

    $this->method = $_SERVER['REQUEST_METHOD'];
    $this->args = $request_url;
    $this->endpoint = $this->args[0];

    if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
      if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
        $this->method = 'DELETE';
      } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
        $this->method = 'PUT';
      } else {
        throw new API_Exception('Unexpected Header', 405);
      }
    }

    switch ($this->method) {
      case 'DELETE':
      case 'POST':
        $this->request = $_POST;
        break;
      case 'GET':
        $this->request = $_GET;
        break;
      case 'PUT':
        $this->request = $_GET;
        $this->file = file_get_contents('php://input');
        break;
      default:
        $response = $this->build_response($this->http_status['405'] . ': ' . $this->method, 405);
        $this->response($response);
    }
  }


  /**
   * Call the requested endpoint.
   *
   * @preturn array with the response
   * @throws API_Exception
   */
  public function processAPI()
  {
    if (method_exists($this, $this->endpoint)) {
      $response = $this->{$this->endpoint}();
      return $this->response($response);
    } else {
      throw new API_Exception('No Endpoint: ' . implode('/', $this->args), 404);
    }
  }

  /**
   * The API response.
   *
   * @param array $response
   * @return array with the response
   */
  private function response(array $response)
  {
    $status = $response['status'];
    header('HTTP/1.1 ' . $status . ' ' . $this->request_status($status));

    return $response['data'] != '' ? json_encode($response['data']) : null;
  }

  /**
   * Get the status code.
   *
   * @param int $code
   * @return int status code
   */
  private function request_status($code)
  {
    $status = $this->http_status;
    return ($status[$code]) ? $status[$code] : $status[500];
  }
}
