<?php

/**
 * Basic Helper class.
 */
abstract class Helper
{
    use Loaders, Utils;
}
