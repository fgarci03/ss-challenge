<?php

/**
 * Abstract Model class.
 */
abstract class Model
{
  use Loaders, Utils;

  /**
   * Controller variables to be used in the view.
   *
   * @var FluentPDO $connection
   */
  private $connection;


  /**
   * Constructor.
   */
  function __construct()
  {
    $dbHelper = $this->load_helper('Database_helper');
    $this->connection = $dbHelper->query();
  }


  /**
   * Query/connection object.
   *
   * @return FluentPDO
   */
  public function query()
  {
    return $this->connection;
  }
}
