<?php

/**
 * REST API
 */
class V1 extends API
{
  /**
   * Constructor.
   *
   * @param array $request_url
   * @param string $origin
   * @throws API_Exception
   */
  public function __construct(array $request_url, $origin)
  {
    parent::__construct($request_url);

    if (!array_key_exists('sToken', $this->request)) {
      throw new API_Exception('No session token provided', 401);
    } elseif ($_SESSION['sToken'] != $this->request['sToken']) {
      throw new API_Exception('Invalid session token', 401);
    } elseif ($_SESSION['address'] != $origin) {
      throw new API_Exception('Address of request is different from the original requester', 403);
    }
  }


  /**
   * mailing-list endpoint.
   *
   * @return array with the API response
   * @throws API_Exception
   */
  protected function mailing_list()
  {
    if (method_exists($this, '_' . $this->args[1])) {
      return $this->{'_' . $this->args[1]}();
    } else {
      throw new API_Exception('No Endpoint: ' . implode('/', $this->args), 404);
    }
  }

  /**
   * add endpoint method - add email to DB.
   *
   * @return array with the API response
   * @throws API_Exception
   */
  private function _add()
  {
    if ($this->method == 'POST') {
      $model = $this->load_model('List_Model');
      $validation_helper = $this->load_helper('Validation_Helper');
      $payload = array(
        $validation_helper->build_payload_item('name', 'name', $this->request['name']),
        $validation_helper->build_payload_item('email', 'email', $this->request['email'])
      );

      $errors = $validation_helper->validate($payload);
      if (sizeof($errors)) {
        throw new API_Exception($errors, 406);
      }

      $success = $model->add_email_to_list($this->request['name'], $this->request['email']);
      if ($success['status'] == 200) {
        return $this->build_response();
      } else {
        throw new API_Exception($success['errors'], $success['status']);
      }
    } else {
      throw new API_Exception('Only accepts POST requests', 405);
    }
  }

  /**
   * show endpoint method - show email list.
   *
   * @return array with the API response
   * @throws API_Exception
   */
  private function _show()
  {
    if ($this->method == 'GET') {
      $model = $this->load_model('List_Model');
      return $this->build_response($model->get_email_list());
    } else {
      throw new API_Exception('Only accepts GET requests', 405);
    }
  }
}
