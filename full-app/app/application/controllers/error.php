<?php

/**
 * Generic error Controller.
 */
class Error extends Controller
{
  /**
   * Render the error template with the header and footer included.
   *
   * @param int $error_code the error code
   * @param string $custom custom error message
   */
  private function render_error($error_code = 0, $custom = '')
  {
    $template_helper = $this->load_helper('template_helper');
    $error_model = $this->load_model('error_model');
    $errors = $error_model->get_errors_list();

    $error_code = $error_code > 0 ? $error_code : 999;

    $template_data = [
      'page_title' => 'Ooooooops!',
      'page_content' => [
        'errors' => $errors[$error_code],
        'custom' => $custom
      ]
    ];

    $template_helper->render_template($this, 'common/error', $template_data);
  }

  /**
   * Render the server's error
   */
  public function index()
  {
    $this->render_error();
  }

  /**
   * Render the 403 "Forbidden" error template
   */
  public function error403()
  {
    $this->render_error(403);
  }

  /**
   * Render the 404 "Not Found" error template
   */
  public function error404()
  {
    $this->render_error(404);
  }

  /**
   * Render the 500 "Internal server error" error template
   */
  public function error500()
  {
    $this->render_error(500);
  }
}
