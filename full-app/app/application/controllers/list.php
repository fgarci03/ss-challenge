<?php

/**
 * List emails page Controller
 */
class _List extends Controller
{
  /**
   * List emails
   */
  public function index()
  {
    $template_helper = $this->load_helper('template_helper');
    $template_data = [
      'page_title' => 'List Emails'
    ];

    $template_helper->render_template($this, 'list', $template_data);
  }
}
