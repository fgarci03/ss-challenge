<?php

/**
 * Add emails page Controller
 */
class Add extends Controller
{
  /**
   * Add email
   */
  public function index()
  {
    $template_helper = $this->load_helper('template_helper');
    $template_data = [
      'page_title' => 'Add Email'
    ];

    $template_helper->render_template($this, 'add', $template_data);
  }
}
