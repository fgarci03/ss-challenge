<?php

/**
 * Homepage Controller
 */
class Home extends Controller
{
  /**
   * Load homepage
   */
  public function index()
  {
    $template_helper = $this->load_helper('template_helper');
    $template_data = [
      'page_title' => 'Home'
    ];

    $template_helper->render_template($this, 'home', $template_data);
  }
}
