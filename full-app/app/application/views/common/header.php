<!doctype html>
<!--[if lte IE 8 ]>
<html lang="en" class="no-js oldie"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>
    <?php echo $controller_data["page_title"]; ?>
  </title>
  <meta name="description" content="Mailing List App">
  <meta name="author" content="Filipe Garcia">
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <link rel="shortcut icon" type="image/x-icon" href="<?php echo BASE_URL; ?>static/favicon.png">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/styles/normalize.css" type="text/css"
        media="screen"/>
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/styles/skeleton.css" type="text/css"
        media="screen"/>
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>static/assets/styles/style.css" type="text/css" media="screen"/>
</head>

<body>
<header>
  <nav class="navbar">
    <div class="container">
      <ul class="navbar-list">
        <li class="navbar-item"><a
            class="navbar-link<?php echo $controller_data['active_link']('home', $controller_data['current_page']); ?>"
            href="<?php echo BASE_URL; ?>">Home</a></li>
        <li class="navbar-item"><a
            class="navbar-link<?php echo $controller_data['active_link']('add', $controller_data['current_page']); ?>"
            href="<?php echo BASE_URL; ?>add">Add Email</a></li>
        <li class="navbar-item"><a
            class="navbar-link<?php echo $controller_data['active_link']('list', $controller_data['current_page']); ?>"
            href="<?php echo BASE_URL; ?>list">List</a></li>
      </ul>
    </div>
  </nav>
</header>
