<div class="container">
  <h1 class="title"><?php echo $controller_data['errors']['title']; ?></h1>

  <p><?php echo $controller_data['errors']['message']; ?></p>

  <?php if ($controller_data['custom'] != '') echo $controller_data['custom']; ?>
</div>
