<div class="container">
  <h1 class="title">Add Email</h1>

  <p>Subscribe to our cool website by filling the form:</p>

  <div class="form-wrapper">
    <div class="overlay"></div>
    <div class="spinner-wrapper">
      <div class="spinner" id="form-spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
      </div>
    </div>

    <form id="AddEmailForm">
      <input type="hidden" id="sToken" value="<?php echo $_SESSION['sToken']; ?>">

      <div class="row">
        <div class="six columns">
          <label for="NameInput">Your name:</label>
          <input class="u-full-width" type="text" placeholder="Awesome name" id="NameInput">
        </div>
        <div class="six columns">
          <label for="EmailInput">Your email:</label>
          <input class="u-full-width" type="email" placeholder="awesome@email.com" id="EmailInput">
        </div>
      </div>
      <div>
        <input type="submit" class="button-primary" value="Submit">
        <input type="reset" value="Reset">
      </div>
    </form>
  </div>
  <span class="hidden" id="feedback"></span>
</div>
