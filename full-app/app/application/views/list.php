<div class="container">
  <h1 class="title">Emails List</h1>

  <p>Check out who subscribed!</p>
  <table class="u-full-width" id="email-list-table">
    <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <div class="spinner" id="spinner">
    <div class="double-bounce1"></div>
    <div class="double-bounce2"></div>
  </div>
  <input type="hidden" id="sToken" value="<?php echo $_SESSION['sToken']; ?>">
</div>
