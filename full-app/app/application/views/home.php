<div class="container">
  <h1 class="title">Hey, welcome!</h1>

  <div>
    <h4>The Problem</h4>

    <p>Make an application which stores names and email addresses in a database.</p>
    <ol>
      <li>
        Has welcome page in http://localhost/
        <ul>
          <li>this page has links to list and create functions</li>
        </ul>
      </li>
      <li>Lists all stored names / email address in http://localhost/list</li>
      <li>
        Adds a name / email address to the database in http://localhost/add
        <ul>
          <li>should validate input and show errors</li>
        </ul>
      </li>
    </ol>
    <p>Since you’re doing an application in make sure the app does not have major security problems: CSRF, XSS, SQL
      Injection.</p>
  </div>

  <div>
    <h4>The Solution/Methodology</h4>
    <ul>
      <li>A basic MVC framework is in use. This structure is an extended version of the <a
          href="https://gilbitron.github.io/PIP" target="_blank">PIP Framework</a>, which I have added composer support,
        and improved overall structure;
      </li>
      <li>Adding and retrieving emails from the database is done with a modified version of <a
          href="http://coreymaynard.com/blog/creating-a-restful-api-with-php" target="_blank">a PHP REST API</a>, which
        is embedded in the framework;
      </li>
      <li>For basic styling, <a href="http://getskeleton.com" target="_blank">skeleton.css</a> is used;</li>
      <li>For Database handling, <a href="https://fpdo.github.io/fluentpdo" target="_blank">FluentPDO</a> seemed
        promising to handle secure and centralized query building (escapes input and uses prepared statements). It was
        wrapped in a helper class that is used at the constructor level of the Model class, in order to provide database
        support to where it belongs, right out of the box!
      </li>
      <li>CSRF is handled with a random generated session token needed on all requests to the API, and the requester's
        IP Address is bounded to the session.
      </li>
    </ul>
  </div>

  <div>
    <h4>Notes:</h4>

    <p>Another possible solution would be to build a single page application and handle all routing through javascript,
      leaving the backend simply with the REST API processing. Despite being a good solution, I figured the goal was to
      evaluate PHP, so I went with this methodology.</p>

    <p>The IP address validation may not be a good solution if the application's target audience sits behind TOR or
      similar software. Still, as a proof of concept, or even in situations where this would not pose a problem, it is a
      good solution to session hijacking.</p>
  </div>
</div>
