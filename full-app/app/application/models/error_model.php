<?php

/**
 * Error Model.
 */
class Error_Model extends Model
{
  /**
   * Get the errors list
   *
   * @return array
   */
  public function get_errors_list()
  {
    $errors = $this->query()
      ->from('errors')
      ->select(array('code', 'title', 'message'))
      ->fetchAll();

    return array_reduce($errors, 'Error_Model::error_list_mapper', array());
  }


  /**
   * Map each error to an associative array
   *
   * @param array $result
   * @param array $error
   * @return array
   */
  private static function error_list_mapper(array $result, array $error)
  {
    $result[$error['code']] = [
      'title' => $error['title'],
      'message' => $error['message']
    ];

    return $result;
  }
}
