<?php

/**
 * List Model.
 */
class List_Model extends Model
{
  /**
   * Get the emails list from the DB.
   *
   * @return array with the mailing list
   */
  public function get_email_list()
  {
    return $this->query()
      ->from('emails_list')
      ->select(array('name', 'email'))
      ->fetchAll();
  }

  /**
   * Add an email to the DB.
   *
   * @param string $name
   * @param string $email
   * @throws API_Exception
   * @return array with the status code
   */
  public function add_email_to_list($name, $email)
  {
    $duplicate = $this->query()
      ->from('emails_list')
      ->where('email', $email)
      ->fetch();

    if (!!$duplicate) {
      $error = [
        'message' => 'Email already in our database!'
      ];
      throw new API_Exception($error, 406);
    }

    $this->query()
      ->insertInto('emails_list')
      ->values(array('name' => $name, 'email' => $email))
      ->execute();

    return [
      'status' => 200
    ];
  }
}
