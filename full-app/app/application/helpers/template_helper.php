<?php

/**
 * Provide generic template methods.
 */
class Template_helper extends Helper
{
  /**
   * Render a template with the header and footer included
   *
   * @param object $view_scope the scope of the caller Controller ($this)
   * @param string $template_name the name of the template to render
   * @param string $template_data the data to render the template with
   */
  public function render_template($view_scope, $template_name, $template_data)
  {
    $header_content = [
      'page_title' => $template_data['page_title'],
      'current_page' => $template_name,
      'active_link' => function ($link, $template_name) {
        return $template_name === $link ? ' active' : '';
      }
    ];

    $footer_script = 'static/assets/scripts/' . $template_name . '.js';
    $footer_content = [
      'script' => file_exists($footer_script) ? BASE_URL . $footer_script : ''
    ];

    $header_template = $view_scope->load_view('common/header');
    $header_template->set('controller_data', $header_content);

    $page_template = $view_scope->load_view($template_name);
    if (isset($template_data['page_content'])) {
      $page_template->set('controller_data', $template_data['page_content']);
    }

    $footer_template = $view_scope->load_view('common/footer');
    $footer_template->set('controller_data', $footer_content);

    $header_template->render();
    $page_template->render();
    $footer_template->render();
  }
}
