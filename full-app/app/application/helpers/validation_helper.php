<?php

/**
 * Helper for data validation.
 */
class Validation_Helper
{
  /**
   * Format an item to an iterable format.
   *
   * @param string $name
   * @param string $type
   * @param string $value
   * @return array
   */
  public static function build_payload_item($name, $type, $value)
  {
    return [
      'name' => $name,
      'type' => $type,
      'value' => $value
    ];
  }

  /**
   * Validate a payload.
   *
   * @param array $payload
   * @return array
   */
  public static function validate(array $payload)
  {
    $errors = array();

    foreach ($payload as $item) {
      switch ($item['type']) {
        case 'name':
          if (!Utils::validate_regexp($item['value'], "/^[A-zÀ-ÿ -.']{3,50}$/")) {
            array_push($errors, $item);
          }
          break;
        case 'email':
          if (!Utils::validate_email($item['value'])) {
            array_push($errors, $item);
          }
          break;
      }
    }
    return $errors;
  }
}
