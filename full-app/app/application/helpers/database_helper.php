<?php

/**
 * Provide database methods.
 */
class Database_helper extends Helper
{
  /**
   * The FluentPDO connection object.
   *
   * @var FluentPDO
   */
  private $connection;


  /**
   * Constructor.
   */
  function __construct()
  {
    try {
      global $config;
      $pdo = new PDO('sqlite:' . $config["db_name"]);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $this->connection = new FluentPDO($pdo);
    } catch (PDOException $e) {
      die('Connection to DB failed: ' . $e->getMessage());
    }
  }


  /**
   * Query/connection object
   *
   * @return FluentPDO
   */
  public function query()
  {
    return $this->connection;
  }
}
