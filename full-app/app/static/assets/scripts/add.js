(function ($) {
  var form = $('#AddEmailForm');

  var nameField = $('#NameInput');
  var emailField = $('#EmailInput');
  var sTokenField = $('#sToken');

  var errorClass = 'error';
  var successClass = 'success';
  var hiddenClass = 'hidden';
  var inputErrorClass = 'input-error';

  function isEmpty(value) {
    return value === undefined || value === null || value === '';
  }

  function validateName(name, allowEmpty) {
    var pattern = new RegExp(/^[A-zÀ-ÿ-. ']{3,50}$/);
    return pattern.test(name) || (allowEmpty ? isEmpty(name) : false);
  }

  function validateEmail(email, allowEmpty) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(email) || (allowEmpty ? isEmpty(email) : false);
  }

  function removeErrors() {
    nameField.removeClass(inputErrorClass);
    emailField.removeClass(inputErrorClass);
    $('#feedback').removeClass(errorClass + ' ' + successClass).addClass(hiddenClass).text('');
  }

  function showSpinner() {
    $('.spinner-wrapper').show();
    $('.overlay').show();
  }

  function hideSpinner() {
    $('.spinner-wrapper').hide();
    $('.overlay').hide();
  }

  nameField.on('input', function () {
    validateName(nameField.val(), true) ? nameField.removeClass(inputErrorClass) : nameField.addClass(inputErrorClass);
  });

  emailField.on('input', function () {
    validateEmail(emailField.val(), true) ? emailField.removeClass(inputErrorClass) : emailField.addClass(inputErrorClass);
  });

  form.on('reset', function () {
    removeErrors();
  });

  form.submit(function (event) {
    event.preventDefault();
    var messageClass, message;
    var errorMessage = 'There are errors in your form!';
    var successMessage = 'Thank you!';

    $.ajax({
      url: 'api/v1/mailing_list/add',
      type: 'POST',
      data: {
        sToken: sTokenField.val(),
        name: nameField.val(),
        email: emailField.val()
      },
      beforeSend: function (xhr) {
        removeErrors();
        showSpinner();
        var nameIsValid = validateName(nameField.val(), false);
        var emailIsValid = validateEmail(emailField.val(), false);

        if (!nameIsValid) {
          nameField.addClass(inputErrorClass);
        }
        if (!emailIsValid) {
          emailField.addClass(inputErrorClass);
        }

        if (!nameIsValid || !emailIsValid) {
          xhr.abort();
          hideSpinner();
          $('#feedback').addClass(errorClass).text(errorMessage).removeClass(hiddenClass);
        }
      },
      success: function () {
        messageClass = successClass;
        message = successMessage;
        form[0].reset();
        hideSpinner();
      },
      error: function (data) {
        message = 'An unexpected error has occured!';
        messageClass = errorClass;
        try {
          var error = JSON.parse(data.responseText).error;
          if (error.message) {
            message = error.message;
          }
        } catch (e) {
          console.error(data);
        }

        hideSpinner();
      },
      complete: function () {
        $('#feedback').addClass(messageClass).text(message).removeClass(hiddenClass);
      }
    });
  })
})(jQuery);
