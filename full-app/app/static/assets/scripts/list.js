(function ($) {
  $.ajax({
    url: 'api/v1/mailing_list/show',
    type: 'GET',
    data: {
      sToken: $('#sToken').val()
    },
    success: function (list) {
      $('#spinner').hide();
      if (list.length) {
        $.each(list, function (i, item) {
          $('#email-list-table')
            .append($('<tr>')
              .append(
              $('<td>').text(item.name),
              $('<td>').html('<a href="mailto:' + item.email + '">' + item.email + '</a>')
            )
          );
        });
      } else {
        $('#email-list-table')
          .append($('<tr>').append($('<td colspan="2">').text('There\'s nothing here =('))
        );
      }
    },
    error: function (error) {
      $('#spinner').hide();
      $('#email-list-table')
        .append($('<tr>')
          .append($('<td colspan="2">').text('There\'s nothing here =('))
      );
      console.error(error);
    }
  });
})(jQuery);
