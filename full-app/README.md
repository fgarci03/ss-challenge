# Mailing List App

### Assumptions
+ PHP version is 5.5 or above;
+ Apache has mod_rewrite enabled;
+ Composer is installed;
+ Mcrypt PHP Extension installed and enabled (most times it's by default, but since I use Linux Mint and it doesn't,
I'd better make sure!).


### Usage
This app has a very simple build process. You just need to clone the repo and ```cd ss-challenge/full-app``` and do
```composer install```. Verify there are no errors and it's ready. Just copy the contents of 
```ss-challenge/full-app/app```  to the root of your server.

You can also change the app's default configs in ```ss-challenge/full-app/fixtures/build.php```. This script is called
by composer after installing the dependencies and is responsible for building the application's config file and database.


### Gotchas:
+ In Linux, the database file and it's parent folder need to be given write permissions (or built directly into the /var/www directory)


### Tested with:
+ Windows 8 -> WAMP Server (PHP v5.6.16)
+ Windows 10 -> WAMP Server (PHP v5.5.12)
+ Linux Mint 17.2 -> LAMP Server (PHP v5.5.9)



Enjoy!
