<?php
/**
 * Build Script
 *
 * Create Database structure and fill it with initial/sample data.
 */

define('APP', 'app/');

$app_config = [
  'base_url' => '/', // Base URL including leading and trailing slash (e.g. / or /app/) - change if not serving from root of the /www/ folder
  'db_name' => 'application/data/db.sqlite',
  'default_controller' => 'home',
  'error_controller' => 'error'
];

$build_config = [
  'load_sample_data' => true // load a few e-mails to the database
];


function generate_ini_file(array $arr)
{
  echo "-> creating 'config.ini'...\n";
  $out = '';
  foreach ($arr as $key => $value) {
    $out .= "$key=$value" . PHP_EOL;
  }

  file_put_contents(APP . 'application/data/config.ini', $out);
}


if (file_exists(APP . $app_config['db_name'])) {
  echo "-> deleting old database...\n";
  unlink(APP . $app_config['db_name']);
}


$tables = [
  [
    'name' => 'errors',
    'sql' => 'CREATE TABLE IF NOT EXISTS errors(
      code      INTEGER     PRIMARY KEY,
      title     TEXT        NOT NULL,
      message   TEXT        NOT NULL
    );'
  ],
  [
    'name' => 'emails_list',
    'sql' => 'CREATE TABLE IF NOT EXISTS emails_list(
      email     TEXT        PRIMARY KEY,
      name      TEXT        NOT NULL
    );'
  ]
];


try {
  echo "-> creating new database...\n";
  $db = new PDO('sqlite:' . APP . $app_config['db_name']);
} catch (PDOException $e) {
  die('Connection to DB failed: ' . $e->getMessage());
}


// create all the tables
foreach ($tables as $table) {
  try {
    echo "-> creating table '" . $table['name'] . "''...\n";
    $db->exec($table['sql']);
  } catch (PDOException $e) {
    die('Error creating table \'' . $table['name'] . ': ' . $e->getMessage());
  }
}


// load errors to DB
echo "-> loading errors to database...\n";
$errors = json_decode(file_get_contents('fixtures/data/errors.json'), true);
foreach ($errors as $error) {
  $db->exec('INSERT INTO errors (code, title, message) VALUES (\'' . $error['code'] . '\', \'' . $error['title'] . '\', \'' . $error['message'] . '\')');
}


if ($build_config['load_sample_data']) {
  // load sample emails to DB
  echo "-> loading sample data...\n";
  $email_list = json_decode(file_get_contents('fixtures/data/sample-emails.json'), true);
  foreach ($email_list as $email) {
    $db->exec('INSERT INTO emails_list (email, name) VALUES (\'' . $email['email'] . '\', \'' . $email['name'] . '\')');
  }
}


$db = null;
generate_ini_file($app_config);
echo "\nAll set! Now copy the contents of the 'app' folder to /www" . $app_config['base_url'] . "\n";
